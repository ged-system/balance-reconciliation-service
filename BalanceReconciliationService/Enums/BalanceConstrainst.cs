﻿namespace BalanceReconciliationService.Enums;

/// <summary>
/// Перечисление граничных условий
/// </summary>
public enum BalanceConstraints
{                  
    /// <summary>
    /// Метрологические
    /// </summary>
    Metrological,
    
    /// <summary>
    /// Технологические
    /// </summary>
    Technological
}