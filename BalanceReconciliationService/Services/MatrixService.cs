﻿using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace BalanceReconciliationService.Services;

/// <summary>
/// Сервис работы с матрицами
/// </summary>
public class MatrixService : IMatrixService
{
    /// <summary>
    /// Сложение матриц
    /// </summary>
    /// <param name="leftMatrix">Левая матрица</param>
    /// <param name="rightMatrix">Права матрица</param>
    /// <returns>Возвращает результат сложения матриц в видел двумерного массива</returns>
    public double[,] Add(double[,]? leftMatrix, double[,]? rightMatrix)
    {
        var result = GetMatrix(leftMatrix) + GetMatrix(rightMatrix);
        return result.ToArray();
    }

    /// <summary>
    /// Умножение матриц
    /// </summary>
    /// <param name="leftMatrix">Левая матрица</param>
    /// <param name="rightMatrix">Права матрица</param>
    /// <returns>Возвращает результат умножения матриц в видел двумерного массива</returns>
    public double[,] Mul(double[,]? leftMatrix, double[,]? rightMatrix)
    {
        var result = GetMatrix(leftMatrix) * GetMatrix(rightMatrix);
        return result.ToArray();
    }

    /// <summary>
    /// Транспонирование матрицы
    /// </summary>
    /// <param name="matrix">Матрица</param>
    /// <returns>Возвращает результат транспонирования матрицы в виде двумерного массива</returns>
    public double[,] Transpose(double[,]? matrix)
    {
        return GetMatrix(matrix).Transpose().ToArray();
    }

    /// <summary>
    /// Получение столбца матрицы
    /// </summary>
    /// <param name="index">Номер столбца</param>
    /// <param name="matrix">Матрица</param>
    /// <returns>Возвращает столбец матрицы в виде одномерного массива</returns>
    public double[] GetColumn(int index, double[,] matrix)
    {
        // Получаем длину строку матрицы
        var length = matrix.GetLength(0);

        var column = new double[length];
        for (var i = 0; i < length; i++)
        {
            column[i] = matrix[i, index];
        }

        return column;
    }
    
    /// <summary>
    /// Возвращает плотную или разреженную матрицу
    /// </summary>
    /// <param name="matrix">Двумерный массив для преобразования в матрицу</param>
    /// <returns>Выполняет преобразование двумерного массива в объект матрицы</returns>
    private Matrix<double> GetMatrix(double[,]? matrix)
    {
        var countOfZeroes = matrix.Cast<double>().Count(value => value == 0);

        var matrixLength = matrix.Length;
        if ((double) countOfZeroes / matrixLength >= 0.7)
        {
            return SparseMatrix.OfArray(matrix);
        }

        return DenseMatrix.OfArray(matrix);
    }
}