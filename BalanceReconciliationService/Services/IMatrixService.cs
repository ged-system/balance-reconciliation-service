﻿namespace BalanceReconciliationService.Services;

/// <summary>
/// Интерфейс сервиса работы с матрицами
/// </summary>
public interface IMatrixService
{
    /// <summary>
    /// Сложение матриц
    /// </summary>
    /// <param name="leftMatrix">Левая матрица</param>
    /// <param name="rightMatrix">Права матрица</param>
    /// <returns>Возвращает результат сложения матриц в видел двумерного массива</returns>
    public double[,] Add(double[,]? leftMatrix, double[,]? rightMatrix);

    /// <summary>
    /// Умножение матриц
    /// </summary>
    /// <param name="leftMatrix">Левая матрица</param>
    /// <param name="rightMatrix">Права матрица</param>
    /// <returns>Возвращает результат умножения матриц в видел двумерного массива</returns>
    public  double[,] Mul(double[,]? leftMatrix, double[,]? rightMatrix);

    /// <summary>
    /// Транспонирование матрицы
    /// </summary>
    /// <param name="matrix">Матрица</param>
    /// <returns>Возвращает результат транспонирования матрицы в виде двумерного массива</returns>
    public double[,] Transpose(double[,]? matrix);

    /// <summary>
    /// Получение столбца матрицы
    /// </summary>
    /// <param name="index">Номер столбца</param>
    /// <param name="matrix">Матрица</param>
    /// <returns>Возвращает столбец матрицы в виде одномерного массива</returns>
    public double[] GetColumn(int index, double[,] matrix);
}