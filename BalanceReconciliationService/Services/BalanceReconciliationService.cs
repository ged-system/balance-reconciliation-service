﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BalanceReconciliationService.Enums;
using BalanceReconciliationService.Helpers;
using BalanceReconciliationService.Models;
using BalanceReconciliationService.Properties;
using Microsoft.AspNetCore.Mvc;

namespace BalanceReconciliationService.Services;

/// <summary>
/// Сервис сведения баланса
/// </summary>
public class BalanceReconciliationService : IBalanceReconciliationService
{
    private readonly IMatrixService _matrixService;

    /// <summary>
    /// Сервис сведения баланса
    /// </summary>
    /// <param name="matrixService">Сервис работы с матрицами</param>
    public BalanceReconciliationService(IMatrixService matrixService)
    {
        _matrixService = matrixService;
    }

    /// <summary>
    /// Выполняет сведение баланса
    /// </summary>
    /// <param name="flows">Коллекция объектов потока</param>
    /// <param name="solver">Солвер сведения баланса</param>
    /// <param name="constraints">Граничные условия</param>
    /// <returns>Возвращает коллекцию объектов потоков</returns>
    [ProducesResponseType(typeof(IEnumerable<Flow>), 200)]
    public BalanceProblemResult SolveBalanceProblem(ICollection<Flow> flows, BalanceSolver solver,
        BalanceConstraints constraints)
    {
        var stopWatch = new Stopwatch();
        stopWatch.Start();

        // Преобразуем потоки в модель системы
        var systemRepresentation = new SystemRepresentation(_matrixService, flows);

        // Получаем солвер сведения баланса
        IBalanceSolver balanceSolver = solver switch
        {
            BalanceSolver.Default => new DefaultSolver(),
            BalanceSolver.Matlab => new MatlabSolver(),
            _ => new DefaultSolver()
        };

        // Если не получилось определить солвер сведения баланса кидаем исключение
        if (balanceSolver == null)
        {
            throw new Exception(ErrorsResources.DefineBalanceSolverError);
        }

        // Получаем граничные условия
        var (lowerBounds, upperBounds) = constraints switch
        {
            BalanceConstraints.Metrological => (
                _matrixService.GetColumn(0, systemRepresentation.MetrologicRangeLowerBound),
                _matrixService.GetColumn(0, systemRepresentation.MetrologicRangeUpperBound)),
            BalanceConstraints.Technological => (
                _matrixService.GetColumn(0, systemRepresentation.TechnologicRangeLowerBound),
                _matrixService.GetColumn(0, systemRepresentation.TechnologicRangeUpperBound)),
            _ => (_matrixService.GetColumn(0, systemRepresentation.MetrologicRangeLowerBound),
                _matrixService.GetColumn(0, systemRepresentation.MetrologicRangeUpperBound)),
        };

        // Выполняем сведение баланса
        var solverRawResult = balanceSolver.Solve(systemRepresentation.H,
            _matrixService.GetColumn(0, systemRepresentation.D),
            systemRepresentation.Aeq,
            _matrixService.GetColumn(0, systemRepresentation.Beq), lowerBounds,
            upperBounds, _matrixService.GetColumn(0, systemRepresentation.X0));
        stopWatch.Stop();

        var reconciledFlowsArray = solverRawResult.Values?.ToArray();
        if (reconciledFlowsArray == null)
        {
            throw new Exception(ErrorsResources.BalanceReconciliationResultError);
        }

        // Собираем результирующую модель
        var resultedFlows = new List<ReconciledFlow>();
        for (var i = 0; i < reconciledFlowsArray.Length; i++)
        {
            var flow = flows.ElementAt(i);

            // Получаем сбалансированное значение потока
            var reconciledMeasured = reconciledFlowsArray[i];

            // Добавляем сбалансированный поток в коллекцию
            resultedFlows.Add(new ReconciledFlow()
            {
                Id = flow.Id,
                Name = flow.Name,
                SourceId = flow.SourceId,
                DestinationId = flow.DestinationId,
                Measured = flow.Measured,
                ReconciledMeasured = reconciledFlowsArray[i],
                Tolerance = flow.Tolerance,
                IsMeasured = flow.IsMeasured,
                IsExcluded = flow.IsExcluded,
                MetrologicRange = flow.MetrologicRange,
                TechnologicRange = flow.TechnologicRange,
                DifferenceValue = reconciledMeasured - flow.Measured,
                IsArtificial = flow.IsArtificial
            });
        }

        return new BalanceProblemResult()
        {
            Flows = resultedFlows,
            Solver = solver,
            ImbalanceBefore = solverRawResult.ImbalanceBefore,
            ImbalanceAfter = solverRawResult.ImbalanceAfter,
            Time = stopWatch.ElapsedMilliseconds
        };
    }
}