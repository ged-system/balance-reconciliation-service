﻿using System.Collections.Generic;
using BalanceReconciliationService.Enums;
using BalanceReconciliationService.Models;

namespace BalanceReconciliationService.Services;

/// <summary>
/// Интерфейс сервиса сведения баланса
/// </summary>
public interface IBalanceReconciliationService
{
    /// <summary>
    /// Выполняет сведение баланса
    /// </summary>
    /// <param name="flows">Коллекция объектов потока</param>
    /// <param name="solver">Солвер сведения баланса</param>
    /// <param name="constraints">Граничные условия</param>
    /// <returns>Возвращает коллекцию объектов потоков</returns>
    public BalanceProblemResult SolveBalanceProblem(ICollection<Flow> flows, BalanceSolver solver, BalanceConstraints constraints);
}