﻿using System.ComponentModel.DataAnnotations;

namespace BalanceReconciliationService.Dtos;

/// <summary>
/// Объект переноса данных потока
/// </summary>
public class FlowDto
{
    /// <summary>
    /// Идентификатор потока
    /// </summary>
    [Required]
    public string? Id { get; set; }

    /// <summary>
    /// Наименование потока
    /// </summary>
    [Required]
    public string? Name { get; set; }

    /// <summary>
    /// Идентификатор узла источника
    /// </summary>
    public string? SourceId { get; set; }

    /// <summary>
    /// Идентификатор узла назначения
    /// </summary>
    public string? DestinationId { get; set; }

    /// <summary>
    /// Измеренное значение потока
    /// </summary>
    [Required]
    public double Measured { get; set; }

    /// <summary>
    /// Погрешность потока
    /// </summary>
    [Required]
    public double Tolerance { get; set; }

    /// <summary>
    /// Флаг, определяющий измеряется ли поток
    /// </summary>
    [Required]
    public bool IsMeasured { get; set; }

    /// <summary>
    /// Флаг, определяющий исключен ли поток из вычислений
    /// </summary>
    [Required]
    public bool IsExcluded { get; set; }

    /// <summary>
    /// Показатель, определяющий, что поток добавлен системой в процессе поиска грубых ошибок
    /// </summary>
    [Required]
    public bool IsArtificial { get; set; }

    /// <summary>
    /// Метрологические граничные условия
    /// </summary>
    [Required]
    public ConstraintsDto? MetrologicRange { get; set; }

    /// <summary>
    /// Технологические граничные условия
    /// </summary>
    [Required]
    public ConstraintsDto? TechnologicRange { get; set; }
}