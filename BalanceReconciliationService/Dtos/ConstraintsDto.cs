﻿using System.ComponentModel.DataAnnotations;

namespace BalanceReconciliationService.Dtos;

/// <summary>
/// Объект переноса данных граничных условий
/// </summary>
public class ConstraintsDto
{
    /// <summary>
    /// Верхняя граница
    /// </summary>
    [Required]
    public double UpperBound { get; set; }

    /// <summary>
    /// Нижняя граница
    /// </summary>
    [Required]
    public double LowerBound { get; set; }
}