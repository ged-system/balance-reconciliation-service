﻿using BalanceReconciliationService.Enums;

namespace BalanceReconciliationService.Dtos;

/// <summary>
/// Объект переноса данных настроек
/// </summary>
public class SettingsDto
{
    /// <summary>
    /// Солвер сведения баланса
    /// </summary>
    public BalanceSolver Solver { get; set; }

    /// <summary>
    /// Граничные условия
    /// </summary>
    public BalanceConstraints Constraints { get; set; }
}