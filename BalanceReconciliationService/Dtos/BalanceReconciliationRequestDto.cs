﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BalanceReconciliationService.Enums;

namespace BalanceReconciliationService.Dtos;

/// <summary>
/// Объект переноса данных запроса сведения баланса
/// </summary>
public class BalanceReconciliationRequestDto
{
    /// <summary>
    /// Коллекция объектов переноса данных потоков
    /// </summary>
    [Required]
    public IEnumerable<FlowDto>? Flows { get; set; }

    /// <summary>
    /// Настройки
    /// </summary>
    [Required]
    public SettingsDto Settings { get; set; } = new SettingsDto()
    {
        Solver = BalanceSolver.Default,
        Constraints = BalanceConstraints.Technological
    };
}