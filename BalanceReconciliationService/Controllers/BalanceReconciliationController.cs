﻿using System;
using System.Linq;
using AutoMapper;
using BalanceReconciliationService.Dtos;
using BalanceReconciliationService.Models;
using BalanceReconciliationService.Properties;
using BalanceReconciliationService.Services;
using Microsoft.AspNetCore.Mvc;

namespace BalanceReconciliationService.Controllers;

/// <summary>
/// Контроллер сведения баланса
/// </summary>
[Route("api/[controller]/[action]")]
[ApiController]
public class BalanceReconciliationController : BalanceReconciliationServiceBaseController
{
    private readonly IMapper _mapper;
    private readonly IBalanceReconciliationService _balanceReconciliationService;

    /// <summary>
    /// Контроллер сведения баланса
    /// </summary>
    /// <param name="mapper">Маппер</param>
    /// <param name="balanceReconciliationService">Сервис сведения баланса</param>
    public BalanceReconciliationController(IMapper mapper, IBalanceReconciliationService balanceReconciliationService)
    {
        _mapper = mapper;
        _balanceReconciliationService = balanceReconciliationService;
    }

    /// <summary>
    /// Выполняет сведение баланса
    /// </summary>
    /// <param name="requestDto">Объект переноса данных запроса сведения баланса</param>
    /// <returns>Возвращает сведенные значения потоков</returns>
    [HttpPost]
    [ProducesResponseType(typeof(BalanceReconciliationResponseDto), 200)]
    public IActionResult SolveBalanceProblem([FromBody] BalanceReconciliationRequestDto requestDto)
    {
        try
        {
            // Получаем коллекцию потоков балансовой схемы
            var requestDtoFlows = requestDto.Flows;

            if (requestDtoFlows == null)
            {
                return BadRequest(string.Format(ErrorsResources.IncorrectParameterError, requestDtoFlows));
            }

            // Получаем коллекцию потоков
            var flows = requestDtoFlows.Select(flow => _mapper.Map<Flow>(flow)).ToList();

            // Получаем солвер
            var solver = requestDto.Settings.Solver;

            // Получаем граничные условия
            var constraints = requestDto.Settings.Constraints;

            var result = _balanceReconciliationService.SolveBalanceProblem(flows, solver, constraints);

            return Ok(_mapper.Map<BalanceReconciliationResponseDto>(result));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }
}