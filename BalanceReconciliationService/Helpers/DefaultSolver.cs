﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using Accord.Math.Optimization;
using BalanceReconciliationService.Models;
using BalanceReconciliationService.Properties;

namespace BalanceReconciliationService.Helpers;

/// <summary>
/// Сервис солвера сведения баланса
/// </summary>
public class DefaultSolver : IBalanceSolver
{
    /// <summary>
    /// Решение задачи сведения баланса
    /// </summary>
    /// <param name="matrixH">Квадратная неотрицательно определенная матрица</param>
    /// <param name="vectorD">Вектор как произведение -H * x0</param>
    /// <param name="matrixAeq">Прямоугольная матрица размером / Левая часть уравнения / Матрица инцидентности</param>
    /// <param name="vectorBeq">Вектор правых частей системы уравнения для ограничения типа равенства</param>
    /// <param name="lowerBounds">Верхние ограничения вектора measuredValues</param>
    /// <param name="upperBound">Нижние ограничения вектора measuredValues</param>
    /// <param name="measuredValues">Вектор измеренных значений</param>
    /// <returns>Объект результата солвера сведения баланса</returns>
    public SolverRawResult Solve(double[,] matrixH, double[] vectorD, double[,] matrixAeq, double[] vectorBeq,
        double[] lowerBounds, double[] upperBound, double[] measuredValues)
    {
        for (var i = 0; i < vectorD.Length; i++)
        {
            vectorD[i] *= -1;
        }

        var constraints = new List<LinearConstraint>();

        // Нижние и верхние границы
        for (var j = 0; j < measuredValues.Length; j++)
        {
            constraints.Add(new LinearConstraint(1)
            {
                VariablesAtIndices = new[] {j},
                ShouldBe = ConstraintType.GreaterThanOrEqualTo,
                Value = lowerBounds[j]
            });

            constraints.Add(new LinearConstraint(1)
            {
                VariablesAtIndices = new[] {j},
                ShouldBe = ConstraintType.LesserThanOrEqualTo,
                Value = upperBound[j]
            });
        }

        // Ограничения для решения задачи баланса
        for (var j = 0; j < vectorBeq.Length; j++)
        {
            var notNullElements = Array.FindAll(matrixAeq.GetRow(j), x => Math.Abs(x) > 0.0000001);
            var notNullElementsIndexes = new List<int>();
            for (var k = 0; k < measuredValues.Length; k++)
            {
                if (Math.Abs(matrixAeq[j, k]) > 0.0000001)
                {
                    notNullElementsIndexes.Add(k);
                }
            }

            constraints.Add(new LinearConstraint(notNullElements.Length)
            {
                VariablesAtIndices = notNullElementsIndexes.ToArray(),
                CombinedAs = notNullElements,
                ShouldBe = ConstraintType.EqualTo,
                Value = vectorBeq[j]
            });
        }

        var function = new QuadraticObjectiveFunction(matrixH, vectorD);

        var solver = new GoldfarbIdnani(function, constraints);
        solver.Minimize();

        if (solver.Status != GoldfarbIdnaniStatus.Success)
        {
            throw new Exception(ErrorsResources.DefaultSolverError);
        }

        return new SolverRawResult()
        {
            Values = solver.Solution.ToList(),
            ImbalanceBefore = GetImbalance(matrixAeq, measuredValues),
            ImbalanceAfter = solver.Value
        };
    }
    
    /// <summary>
    /// Возвращает значение дисбаланса
    /// </summary>
    /// <param name="matrix">Матрица инцидентности</param>
    /// <param name="vector">Вектор значений</param>
    private static double GetImbalance(double[,] matrix, IReadOnlyList<double> vector)
    {
        var sum = 0.0;
        for (var i = 0; i < matrix.GetLength(1); i++)
        {
            for (var j = 0; j < matrix.GetLength(0); j++)
            {
                sum += matrix[j, i] * vector[i];
            }
        }
        return sum;
    }
}