﻿using BalanceReconciliationService.Models;

namespace BalanceReconciliationService.Helpers;

/*
* Солвер сведения баланса
*
* Задача QP состоит в минимизации квадратичной функции
*
* f(x) = 1/2 * x† * H * x + d† * x
* 
* d = -H * x0
* при ограничениях типа равенства:
* A * x = y
*
* и типа неравенства:
* l <= x <= u
*
* x - настраиваемый вектор размера n
* H - квадратная неотрицательно определенная матрица размера n * m
* A - прямоугольная матрица размером m * n (m < n) системы уравнения для ограничений типа равенства
* y - вектор правых частей системы уравнения для ограничения типа равенства
* l, u - векторы размера n, которые являются верхними и нижними ограничениями для компонент вектора x
*/

/// <summary>
/// Интерфейс солвера сведения баланса
/// </summary>
public interface IBalanceSolver
{
    /// <summary>
    /// Решение задачи сведения баланса
    /// </summary>
    /// <param name="matrixH">Квадратная неотрицательно определенная матрица</param>
    /// <param name="vectorD">Вектор как произведение -H * x0</param>
    /// <param name="matrixAeq">Прямоугольная матрица размером / Левая часть уравнения / Матрица инцидентности</param>
    /// <param name="vectorBeq">Вектор правых частей системы уравнения для ограничения типа равенства</param>
    /// <param name="lowerBounds">Верхние ограничения вектора measuredValues</param>
    /// <param name="upperBound">Нижние ограничения вектора measuredValues</param>
    /// <param name="measuredValues">Вектор измеренных значений</param>
    /// <returns>Объект результата солвера сведения баланса</returns>
    SolverRawResult Solve(double[,] matrixH, double[] vectorD, double[,] matrixAeq, double[] vectorBeq,
        double[] lowerBounds, double[] upperBound, double[] measuredValues);
}