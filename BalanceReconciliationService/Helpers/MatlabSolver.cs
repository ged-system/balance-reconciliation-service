﻿using System;
using System.Collections.Generic;
using BalanceReconciliationService.Models;
using BalanceReconciliationService.Properties;
using MathWorks.MATLAB.NET.Arrays;
using MatlabBalanceSolverLibrary;

namespace BalanceReconciliationService.Helpers;

/// <summary>
/// Солвер сведения баланса с использованием Matlab Runtime R2021a
/// </summary>
public class MatlabSolver : IBalanceSolver
{
    private readonly BalanceSolver _matlabWorker;

    /// <summary>
    /// Солвер сведения баланса с использованием Matlab Runtime R2021a
    /// </summary>
    public MatlabSolver()
    {
        _matlabWorker = new BalanceSolver();
    }

    /// <summary>
    /// Решение задачи сведения баланса
    /// </summary>
    /// <param name="matrixH">Квадратная неотрицательно определенная матрица</param>
    /// <param name="vectorD">Вектор как произведение -H * x0</param>
    /// <param name="matrixAeq">Прямоугольная матрица размером / Левая часть уравнения / Матрица инцидентности</param>
    /// <param name="vectorBeq">Вектор правых частей системы уравнения для ограничения типа равенства</param>
    /// <param name="lowerBounds">Верхние ограничения вектора measuredValues</param>
    /// <param name="upperBound">Нижние ограничения вектора measuredValues</param>
    /// <param name="measuredValues">Вектор измеренных значений</param>
    /// <returns>Объект результата солвера сведения баланса</returns>
    public SolverRawResult Solve(double[,] matrixH, double[] vectorD, double[,] matrixAeq, double[] vectorBeq,
        double[] lowerBounds, double[] upperBound, double[] measuredValues)
    {
        for (var i = 0; i < vectorD.Length; i++)
        {
            vectorD[i] *= -1;
        }

        var matrixHNumericArray = new MWNumericArray(matrixH);
        var vectorDNumericArray = new MWNumericArray(vectorD);

        var matrixAeqNumericArray = new MWNumericArray(matrixAeq);
        var vectorBeqNumericArray = new MWNumericArray(vectorBeq);
        var lowerBoundsNumericArray = new MWNumericArray(lowerBounds);
        var upperBoundNumericArray = new MWNumericArray(upperBound);

        var measuredValuesNumericArray = new MWNumericArray(measuredValues);

        var maxIter = new MWNumericArray(20000);
        var DrTol = new MWNumericArray(200);

        // Выполняем сведение баланса
        var result = _matlabWorker.QPSolver(3, matrixHNumericArray, vectorDNumericArray, matrixAeqNumericArray,
            vectorBeqNumericArray, lowerBoundsNumericArray, upperBoundNumericArray, measuredValuesNumericArray,
            maxIter);
        if (result == null)
        {
            throw new Exception(ErrorsResources.MatlabSolverError);
        }

        if (result[0].IsEmpty || result[1].IsEmpty)
        {
            throw new Exception(ErrorsResources.MatlabSolverError);
        }

        // Получаем значения потоков после сведения баланса
        var flows = (double[,]) result[0].ToArray();
        if (flows == null)
        {
            throw new Exception(ErrorsResources.MatlabSolverError);
        }

        // Получаем значение дисбаланса
        // var imbalance = (double[,]) result[1].ToArray();
        // if (imbalance == null)
        // {
        //     throw new Exception(ErrorsResources.MatlabSolverError);
        // }

        var output = new double[flows.Length];
        for (var i = 0; i < flows.Length; i++)
        {
            output[i] = flows[i, 0];
        }

        return new SolverRawResult()
        {
            Values = output,
            ImbalanceBefore = GetImbalance(matrixAeq, measuredValues),
            ImbalanceAfter = GetImbalance(matrixAeq, output)
        };
    }
    
    /// <summary>
    /// Возвращает значение дисбаланса
    /// </summary>
    /// <param name="matrix">Матрица инцидентности</param>
    /// <param name="vector">Вектор значений</param>
    private static double GetImbalance(double[,] matrix, IReadOnlyList<double> vector)
    {
        var sum = 0.0;
        for (var i = 0; i < matrix.GetLength(1); i++)
        {
            for (var j = 0; j < matrix.GetLength(0); j++)
            {
                sum += matrix[j, i] * vector[i];
            }
        }
        return sum;
    }
}