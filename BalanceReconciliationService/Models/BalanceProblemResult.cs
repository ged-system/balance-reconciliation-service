﻿using System.Collections.Generic;
using BalanceReconciliationService.Enums;

namespace BalanceReconciliationService.Models;

/// <summary>
/// Объект результата работы метода сведения баланса
/// </summary>
public class BalanceProblemResult
{
    /// <summary>
    /// Коллекция сбалансированных значений потоков
    /// </summary>
    public IEnumerable<ReconciledFlow>? Flows { get; set; }

    /// <summary>
    /// Солвер сведения баланса
    /// </summary>
    public BalanceSolver Solver { get; set; }

    /// <summary>
    /// Значение дисбаланса до сведения баланса
    /// </summary>
    public double ImbalanceBefore { get; set; }
    
    /// <summary>
    /// Значение дисбаланса после сведения баланса
    /// </summary>
    public double ImbalanceAfter { get; set; }

    /// <summary>
    /// Время затраченное на сведение баланса в мс
    /// </summary>
    public double Time { get; set; }
}