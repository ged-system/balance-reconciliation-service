﻿namespace BalanceReconciliationService.Models;

/// <summary>
/// Объект потока
/// </summary>
public class Flow
{
    /// <summary>
    /// Идентификатор потока
    /// </summary>
    public string? Id { get; set; }

    /// <summary>
    /// Наименование потока
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// Идентификатор узла источника
    /// </summary>
    public string? SourceId { get; set; }

    /// <summary>
    /// Идентификатор узла назначения
    /// </summary>
    public string? DestinationId { get; set; }

    /// <summary>
    /// Измеренное значение потока
    /// </summary>
    public double Measured { get; set; }

    /// <summary>
    /// Погрешность потока
    /// </summary>
    public double Tolerance { get; set; }

    /// <summary>
    /// Флаг, определяющий измеряется ли поток
    /// </summary>
    public bool IsMeasured { get; set; }

    /// <summary>
    /// Флаг, определяющий исключен ли поток из вычислений
    /// </summary>
    public bool IsExcluded { get; set; }
    
    /// <summary>
    /// Показатель, определяющий, что поток добавлен системой в процессе поиска грубых ошибок
    /// </summary>
    public bool IsArtificial { get; set; }
    
    /// <summary>
    /// Метрологические граничные условия
    /// </summary>
    public Constraints? MetrologicRange { get; set; }

    /// <summary>
    /// Технологические граничные условия
    /// </summary>
    public Constraints? TechnologicRange { get; set; }
}