﻿using System;
using System.Collections.Generic;
using System.Linq;
using BalanceReconciliationService.Services;

namespace BalanceReconciliationService.Models;

/// <summary>
/// Объект представления системы
/// </summary>
public class SystemRepresentation
{
    /// <summary>
    /// Объект представления системы
    /// </summary>
    /// <param name="matrixService">Сервис работы с матрицами</param>
    /// <param name="flows">Коллекция объектов потоков</param>
    public SystemRepresentation(IMatrixService matrixService, IEnumerable<Flow> flows)
    {
        if (matrixService == null)
        {
            throw new Exception("Не удалось получить _matrixService");
        }
        
        var flowList = flows.ToList();

        // Определяем количество потоков
        var countOfThreads = flowList.Count();

        // Инициализация матрицы инцидентности ( A )
        Aeq = GetIncidenceMatrix(flowList);

        // Инициализация вектора измеренных значений ( x0 )
        X0 = new double[countOfThreads, 1];

        // Инициализация матрицы измеряемости ( I )
        I = new double[countOfThreads, countOfThreads];

        // Инициализация матрицы метрологической погрешности ( 1 / t * t )
        W = new double[countOfThreads, countOfThreads];

        // Инициализация матрицы абсолютной погрешности
        AbsTol = new double[countOfThreads, countOfThreads];

        // Инициализация вектора верхних ограничений вектора x
        MetrologicRangeUpperBound = new double[countOfThreads, 1];
        TechnologicRangeUpperBound = new double[countOfThreads, 1];

        // Инициализация вектора нижних ограничений вектора x
        MetrologicRangeLowerBound = new double[countOfThreads, 1];
        TechnologicRangeLowerBound = new double[countOfThreads, 1];

        for (var i = 0; i < countOfThreads; i++)
        {
            var variable = flowList.ElementAt(i);
            // Определение вектора измеренных значений
            X0[i, 0] = variable.Measured;

            // Определение матрицы измеряемости
            I[i, i] = variable.IsMeasured ? 1.0 : 0.0;

            // Определение матрицы метрологической погрешности
            if (!variable.IsMeasured)
            {
                W[i, i] = 1.0;
            }
            else
            {
                var inaccuracy = variable.Tolerance == 0.0 ? MinimumInaccuracy : variable.Tolerance;
                W[i, i] = 1.0 / Math.Pow(inaccuracy, 2);
            }

            AbsTol[i, i] = variable.Tolerance;
            // Определение вектора верхних ограничений вектора x
            MetrologicRangeUpperBound[i, 0] = variable.MetrologicRange?.UpperBound ?? 0;
            TechnologicRangeUpperBound[i, 0] = variable.TechnologicRange?.UpperBound ?? 0;

            // Определение вектора нижних ограничений вектора x
            MetrologicRangeLowerBound[i, 0] = variable.MetrologicRange?.LowerBound ?? 0;
            TechnologicRangeLowerBound[i, 0] = variable.TechnologicRange?.LowerBound ?? 0;
        }

        H = matrixService.Mul(I, W);
        D = matrixService.Mul(H, X0);

        Beq = new double[Aeq.GetLength(0), Aeq.GetLength(1)];
    }

    /// <summary>
    /// Матрица измеряемости (I)
    /// </summary>
    public double[,] I { get; }

    /// <summary>
    /// Матрица метрологической погрешности (W)
    /// </summary>
    public double[,] W { get; }

    /// <summary>
    /// Матрица инцидентности / связей
    /// </summary>
    public double[,] Aeq { get; }

    /// <summary>
    /// Вектор сбалансированных значений (x) / Правая часть уравнения
    /// </summary>
    public double[,] Beq { get; }

    /// <summary>
    /// Вектор измеренных значений (x0)
    /// </summary>
    public double[,] X0 { get; }

    /// <summary>
    /// Вектор верхних метрологических ограничений
    /// </summary>
    public double[,] MetrologicRangeUpperBound { get; }

    /// <summary>
    /// Вектор нижних метрологических ограничений
    /// </summary>
    public double[,] MetrologicRangeLowerBound { get; }

    /// <summary>
    /// Вектор верхних технологических ограничений
    /// </summary>
    public double[,] TechnologicRangeUpperBound { get; }

    /// <summary>
    /// Вектор верхних технологических ограничений
    /// </summary>
    public double[,] TechnologicRangeLowerBound { get; }

    /// <summary>
    /// H = I * W
    /// </summary>
    public double[,] H { get; }

    /// <summary>
    /// D = H * x0
    /// </summary>
    public double[,] D { get; }

    /// <summary>
    /// Матрица абсолютной погрешности
    /// </summary>
    public double[,] AbsTol { get; }

    /// <summary>
    /// Минимальная погрешность
    /// </summary>
    private const double MinimumInaccuracy = 1.0E-3;

    /// <summary>
    /// Возвращает вершины графа
    /// </summary>
    /// <param name="flows">Коллекция входных потоков</param>
    /// <returns>Массив узлов графа</returns>
    private Vertex[] GetVertices(IEnumerable<Flow> flows)
    {
        var vertices = new List<Vertex>();

        foreach (var flow in flows)
        {
            if (flow.SourceId == null || flow.DestinationId == null)
            {
                continue;
            }

            if (vertices.All(x => x.Name != flow.SourceId))
            {
                vertices.Add(new Vertex(flow.SourceId));
            }

            if (vertices.All(x => x.Name != flow.DestinationId))
            {
                vertices.Add(new Vertex(flow.DestinationId));
            }
        }

        return vertices.ToArray();
    }

    /// <summary>
    /// Возвращает грани графа
    /// </summary>
    /// <param name="flows">Коллекция входных потоков</param>
    /// <returns>Массив вершин графа</returns>
    private Edge[] GetEdges(IEnumerable<Flow> flows)
    {
        var edges = flows.Select(x => new Edge(x.Id!, x.Name!, x.SourceId!, x.DestinationId!));

        return edges.ToArray();
    }

    /// <summary>
    /// Получение матрицы инцидентности
    /// </summary>
    /// <param name="flows">Коллекция входных потоков</param>
    /// <returns>Двумерный массив матрицы инцидентности графа</returns>
    private double[,] GetIncidenceMatrix(IEnumerable<Flow> flows)
    {
        var vertices = GetVertices(flows).Where(e => e.Name != null).ToArray();

        var edges = GetEdges(flows).ToArray();

        var vertexCount = vertices.Count();
        var edgeCount = edges.Count();

        /*
         * Выполнение кода без учета зависимостей
         */

        var vertexCountMod = vertexCount;

        var result = new double[vertexCountMod, edgeCount];
        
        for (var i = 0; i < vertexCountMod; i++)
        {
            if (i < vertexCount)
            {
                // Определение графа системы
                for (var j = 0; j < edgeCount; j++)
                {
                    if (vertices[i].Name == edges[j].End)
                    {
                        result[i, j] = 1.0;
                    }
                    else if (vertices[i].Name == edges[j].Start)
                    {
                        result[i, j] = -1.0;
                    }
                    else
                    {
                        result[i, j] = 0.0;
                    }
                }
            }
        }

        return result;
    }

    /// <summary>
    /// Объект грани
    /// </summary>
    private class Edge
    {
        /// <summary>
        /// Идентификатор грани
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Наименование грани
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Идентификатор узла источника
        /// </summary>
        public string Start { get; }

        /// <summary>
        /// Идентификатор узла назначения
        /// </summary>
        public string End { get; }

        public Edge(string id, string name, string start, string end)
        {
            Id = id;
            Name = name;
            Start = start;
            End = end;
        }
    }

    /// <summary>
    /// Объект вершины
    /// </summary>
    private class Vertex
    {
        /// <summary>
        /// Наименование вершины
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Объект вершины
        /// </summary>
        /// <param name="name"></param>
        public Vertex(string name)
        {
            Name = name;
        }
    }
}