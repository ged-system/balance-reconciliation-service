﻿using System.Collections.Generic;

namespace BalanceReconciliationService.Models;

/// <summary>
/// Объект результата солвера сведения баланса
/// </summary>
public class SolverRawResult
{
    /// <summary>
    /// Коллекция сбалансированных значения
    /// </summary>
    public IEnumerable<double>? Values { get; set; }
    
    /// <summary>
    /// Значение дисбаланса до сведения баланса
    /// </summary>
    public double ImbalanceBefore { get; set; }
    
    /// <summary>
    /// Значение дисбаланса после сведения баланса
    /// </summary>
    public double ImbalanceAfter { get; set; }
}