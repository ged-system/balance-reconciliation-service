﻿namespace BalanceReconciliationService.Models;

/// <summary>
/// Объект сбалансированного потока
/// </summary>
public class ReconciledFlow : Flow
{
    /// <summary>
    /// Значение потока после сведения баланса
    /// </summary>
    public double ReconciledMeasured { get; set; }
    
    /// <summary>
    /// Величина изменения между исходным значением потока и сбалансированным
    /// </summary>
    public double DifferenceValue { get; set; }
}