﻿using AutoMapper;
using BalanceReconciliationService.Dtos;
using BalanceReconciliationService.Models;

namespace BalanceReconciliationService.Profiles;

/// <summary>
/// Профайлер объектов сервиса
/// </summary>
public class BalanceReconciliationProfile : Profile
{
    /// <summary>
    /// Профайлер объектов сервиса
    /// </summary>
    public BalanceReconciliationProfile()
        {
            // Source -> Target
            CreateMap<FlowDto, Flow>();
            CreateMap<ConstraintsDto, Constraints>();
            CreateMap<ReconciledFlow, ReconciledFlowDto>();
            CreateMap<BalanceProblemResult, BalanceReconciliationResponseDto>();
        }
}