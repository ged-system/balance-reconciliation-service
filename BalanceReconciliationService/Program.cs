using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text.Json.Serialization;
using BalanceReconciliationService.Properties;
using BalanceReconciliationService.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Linq;

var builder = WebApplication.CreateBuilder(args);

#region Настройка сервисов

#region Добавление Urls в конфигурации

// Проверяем флаг службы
var isService = args.Contains("-service");

// Устанавливаем хост
var host = isService && args.Length >= 3
    ? args[1]
    : string.Empty;

// Устанавливаем порт
var port = isService && args.Length >= 3
    ? args[2]
    : string.Empty;

// Если не в режиме отладки, не сервис и часть конфигурации не заполнена, то спрашиваем параметры у пользователя
if (!Debugger.IsAttached && !isService && (string.IsNullOrWhiteSpace(host) || string.IsNullOrWhiteSpace(port)))
{
    Console.Write(Resource.EnterHost);
    host = Console.ReadLine();
    Console.Write(Resource.EnterPort);
    port = Console.ReadLine();
}

// Если часть параметров осталась не заполнена, то устанавливаем значения по умолчанию
if (string.IsNullOrWhiteSpace(host) || string.IsNullOrWhiteSpace(port))
{
    host = Resource.ApplicationDefaultHost;
    port = Resource.ApplicationDefaultPort;
}

// Добавляем адреса
var urls = $"http://{Resource.ApplicationDefaultHost}:{port}";
if (host != Resource.ApplicationDefaultHost)
{
    urls += $";http://{host}:{port}";
}

// Добавляем Urls в конфиг
builder.WebHost.UseUrls(urls);
#endregion

// Добавление сервисов

builder.Services.AddControllers()
    // Добавляем поддержку чтению перечислений в виде строк
    .AddJsonOptions(opt => opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddScoped<IMatrixService, MatrixService>();
builder.Services.AddScoped<IBalanceReconciliationService, BalanceReconciliationService.Services.BalanceReconciliationService>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(opt =>
{
    opt.SwaggerDoc("v1", new OpenApiInfo {Title = "My API", Version = "v1"});

    var fileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var filePath = Path.Combine(AppContext.BaseDirectory, fileName);
    opt.IncludeXmlComments(filePath);
});

#endregion

var app = builder.Build();

app.UseStaticFiles();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(opt =>
    {
        opt.SwaggerEndpoint("v1/swagger.json", "My API V1");
        opt.InjectStylesheet("/Swagger-ui/swagger-ui.css");
    });
}

app.UseRouting();

app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

app.Run();